<?php

namespace Drupal\geographically_closest_pairs\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class GeographicallyClosestPairsController.
 */
class GeographicallyClosestPairsController extends ControllerBase {
  
  /**
   * EntityTypeBundleInfo service.
   *
   * @var \Drupal\Core\Entity\EntityTypeBundleInfo
   */
  protected $entityTypeBundleInfo;

  /**
   * Entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->entityTypeBundleInfo = $container->get('entity_type.bundle.info');
    $instance->entityTypeManager = $container->get('entity_type.manager');
    $instance->entityFieldManager = $container->get('entity_field.manager');
    return $instance;
  }

  /**
   * Displayclosestpairs.
   *
   * @return string
   *   Return Hello string.
   */
  public function displayClosestPairs(string $content_type_1, string $content_type_2) {
    
    $content_type_1 = strtolower($content_type_1);
    $content_type_2 = strtolower($content_type_2);

    $content_types = $this->entityTypeBundleInfo->getBundleInfo('node');
    
    if (!array_key_exists($content_type_1, $content_types)) {
      $this->messenger()
        ->addWarning($this->t('Content type "@content_type" does not exist.', ['@content_type' => $content_type_1]));
      return [];
    }

    if (!array_key_exists($content_type_2, $content_types)) {
      $this->messenger()
        ->addWarning($this->t('Content type "@content_type" does not exist.', ['@content_type' => $content_type_2]));
      return [];
    }

    // Check if the content types have the field of the 'Geolocation' type.
    $fields_of_content_type_1 = $this->entityFieldManager->getFieldDefinitions('node', $content_type_1);
    
    foreach ($fields_of_content_type_1 as $field) {
      if ($field->getType() == 'geolocation') {
        // Store the name of the field of the 'Geolocation' type.
        $geolocation_field_name_1 = $field->getName();
      }
    }
 
    if (empty($geolocation_field_name_1)) {
      
      $arg = ['@content_type' => $content_type_1, '@geolocation' => 'geolocation'];
      $this->messenger()
        ->addWarning($this->t('Content type "@content_type" does not have a field of the "@geolocation" type.', $arg));
      return [];
    }
    
    $fields_of_content_type_2 = $this->entityFieldManager->getFieldDefinitions('node', $content_type_2);
    
    foreach ($fields_of_content_type_2 as $field) {
      if ($field->getType() == 'geolocation') {
        // Store the name of the field of the 'Geolocation' type.
        $geolocation_field_name_2 = $field->getName();
      }
    }

    if (empty($geolocation_field_name_2)) {
      
      $arg = ['@content_type' => $content_type_2, '@geolocation' => 'geolocation'];
      $this->messenger()
        ->addWarning($this->t('Content type "@content_type" does not have a field of the "@geolocation" type.', $arg));
      return [];
    }

    $query = $this->entityTypeManager->getStorage('node')->getQuery();
    
    $nids = $query->condition('type', $content_type_1)
      ->condition('status', '1')
      ->execute();
    
    $nodes_of_content_type_1 = $this->entityTypeManager->getStorage('node')->loadMultiple($nids);
    
    $query = $this->entityTypeManager->getStorage('node')->getQuery();
    $nids = $query->condition('type', $content_type_2)
      ->condition('status', '1')
      ->execute();

    $nodes_of_content_type_2 = $this->entityTypeManager->getStorage('node')->loadMultiple($nids);
    
    $first_node_of_content_type_2 = reset($nodes_of_content_type_2);
    $sorted_nodes_of_content_type_2 = [];

    foreach ($nodes_of_content_type_1 as $node_1) {

      $lat1 = $node_1->get($geolocation_field_name_1)->getValue()[0]['lat'];
      $lon1 = $node_1->get($geolocation_field_name_1)->getValue()[0]['lng'];
      
      $lat2 = $first_node_of_content_type_2->get($geolocation_field_name_2)->getValue()[0]['lat'];
      $lon2 = $first_node_of_content_type_2->get($geolocation_field_name_2)->getValue()[0]['lng'];
      
      $distance = $this->distance($lat1, $lon1, $lat2, $lon2);
      $node_closest = $first_node_of_content_type_2;

      foreach ($nodes_of_content_type_2 as $node_2) {

        $lat2 = $node_2->get($geolocation_field_name_2)->getValue()[0]['lat'];
        $lon2 = $node_2->get($geolocation_field_name_2)->getValue()[0]['lng'];
        $new_distance = $this->distance($lat1, $lon1, $lat2, $lon2);

        if ($new_distance < $distance) {

          $distance = $new_distance;
          $node_closest = $node_2;
        }
      }

      $sorted_nodes_of_content_type_2[] = $node_closest;
    }

    return [
      '#theme' => 'geographically_closest_pairs',
      '#nodes_of_content_type_1' => $nodes_of_content_type_1,
      '#nodes_of_content_type_2' => $sorted_nodes_of_content_type_2,
      '#geolocation_field_name_1' => $geolocation_field_name_1,
      '#geolocation_field_name_2' => $geolocation_field_name_2,
    ];
  }


  /**
   * Calculates the distance between two points.
   *
   * @return number
   *   The distance in statute miles.
   */
  private function distance($lat1, $lon1, $lat2, $lon2) {
    if (($lat1 == $lat2) && ($lon1 == $lon2)) {
      return 0;
    }
    else {
      $theta = $lon1 - $lon2;
      $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
      $dist = acos($dist);
      $dist = rad2deg($dist);
      $miles = $dist * 60 * 1.1515;
      return $miles;
    }
  }

}
